﻿using System.ComponentModel.DataAnnotations;

namespace btapltrinhweb.Models
{
    public class Reports
    {
        [Key]
        public int ReportID {  get; set; }
        public int AccountID { get; set; }
        public Accounts? Accounts { get; set; }
        public int LogsID { get; set; }
        public Logs? Logs { get; set; }
        public int TransactionalID { get; set; }
        public Transactions? Transactions { get; set; }
        public string Reportname {  get; set; }
        public DateOnly Date { get; set; }

    }
}
