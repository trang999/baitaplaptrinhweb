﻿using System.ComponentModel.DataAnnotations;

namespace btapltrinhweb.Models
{
    public class Accounts
    {
        [Key]
        public int AccountID {  get; set; }
        public int CustumerID {  get; set; }
        public Customer? Customer { get; set; }
        public string AccountName { get; set; }
    }
}
