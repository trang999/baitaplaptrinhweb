﻿using System.ComponentModel.DataAnnotations;

namespace btapltrinhweb.Models
{
    public class Logs
    {
        [Key]
        public int LogsID {  get; set; }
        public int TransactionalID {  get; set; }
        public Transactions? Transactions { get; set; }
        public TimeOnly Logintime { get; set; }
        public DateOnly LoginDate { get; set; }
    }
}
