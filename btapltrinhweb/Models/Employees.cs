﻿using System.ComponentModel.DataAnnotations;

namespace btapltrinhweb.Models
{
    public class Employees
    {
        [Key]
        public int EmployeeID { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string ContactandAddress { get; set; }
        public string UsernameandPassword{ get; set; }
    }
}
