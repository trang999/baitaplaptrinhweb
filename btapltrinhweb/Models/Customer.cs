﻿using System.ComponentModel.DataAnnotations;

namespace btapltrinhweb.Models
{
    public class Customer
    {
        [Key]
        public int CustumerID { get; set; }
        public string Firstname {  get; set; }
        public string Lastname { get; set; }
        public string ContactandAddress { get; set; }
        public string Username {  get; set; }
        public string Password {  get; set; }
    }
}
