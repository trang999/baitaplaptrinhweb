﻿using Microsoft.EntityFrameworkCore;

namespace btapltrinhweb.Models
{
    public class ApplicationDbContext :DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Transactions> Transactions { get; set; }
        public DbSet<Employees> Employeess { get; set; }
        public DbSet<Logs> Logss { get; set; }
        public DbSet<Reports> Reports { get; set; }
        public DbSet<Accounts> Accounts { get; set; }
    }
}
