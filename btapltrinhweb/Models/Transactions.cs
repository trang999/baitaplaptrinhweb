﻿using System.ComponentModel.DataAnnotations;

namespace btapltrinhweb.Models
{
    public class Transactions
    {
        [Key]
        public int TransactionalID { get; set; }
        public int EmployeeID {  get; set; }
        public Employees? Employees { get; set; }
        public int CustomerID {  get; set; }
        public Customer? Customer { get; set; }
        public string Name { get; set; }
        
    }
}
