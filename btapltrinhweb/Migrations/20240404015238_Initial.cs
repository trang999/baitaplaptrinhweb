﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace btapltrinhweb.Migrations
{
    /// <inheritdoc />
    public partial class Initial : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Customers",
                columns: table => new
                {
                    CustumerID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Firstname = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Lastname = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ContactandAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Username = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Password = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Customers", x => x.CustumerID);
                });

            migrationBuilder.CreateTable(
                name: "Employeess",
                columns: table => new
                {
                    EmployeeID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Firstname = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Lastname = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    ContactandAddress = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    UsernameandPassword = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Employeess", x => x.EmployeeID);
                });

            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    AccountID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    CustumerID = table.Column<int>(type: "int", nullable: false),
                    CustomerCustumerID = table.Column<int>(type: "int", nullable: true),
                    AccountName = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.AccountID);
                    table.ForeignKey(
                        name: "FK_Accounts_Customers_CustomerCustumerID",
                        column: x => x.CustomerCustumerID,
                        principalTable: "Customers",
                        principalColumn: "CustumerID");
                });

            migrationBuilder.CreateTable(
                name: "Transactions",
                columns: table => new
                {
                    TransactionalID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    EmployeeID = table.Column<int>(type: "int", nullable: false),
                    EmployeesEmployeeID = table.Column<int>(type: "int", nullable: true),
                    CustomerID = table.Column<int>(type: "int", nullable: false),
                    Name = table.Column<string>(type: "nvarchar(max)", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Transactions", x => x.TransactionalID);
                    table.ForeignKey(
                        name: "FK_Transactions_Customers_CustomerID",
                        column: x => x.CustomerID,
                        principalTable: "Customers",
                        principalColumn: "CustumerID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Transactions_Employeess_EmployeesEmployeeID",
                        column: x => x.EmployeesEmployeeID,
                        principalTable: "Employeess",
                        principalColumn: "EmployeeID");
                });

            migrationBuilder.CreateTable(
                name: "Logss",
                columns: table => new
                {
                    LogsID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    TransactionalID = table.Column<int>(type: "int", nullable: false),
                    TransactionsTransactionalID = table.Column<int>(type: "int", nullable: true),
                    Logintime = table.Column<TimeOnly>(type: "time", nullable: false),
                    LoginDate = table.Column<DateOnly>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Logss", x => x.LogsID);
                    table.ForeignKey(
                        name: "FK_Logss_Transactions_TransactionsTransactionalID",
                        column: x => x.TransactionsTransactionalID,
                        principalTable: "Transactions",
                        principalColumn: "TransactionalID");
                });

            migrationBuilder.CreateTable(
                name: "Reports",
                columns: table => new
                {
                    ReportID = table.Column<int>(type: "int", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    AccountID = table.Column<int>(type: "int", nullable: false),
                    AccountsAccountID = table.Column<int>(type: "int", nullable: true),
                    LogsID = table.Column<int>(type: "int", nullable: false),
                    TransactionalID = table.Column<int>(type: "int", nullable: false),
                    TransactionsTransactionalID = table.Column<int>(type: "int", nullable: true),
                    Reportname = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Date = table.Column<DateOnly>(type: "date", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Reports", x => x.ReportID);
                    table.ForeignKey(
                        name: "FK_Reports_Accounts_AccountsAccountID",
                        column: x => x.AccountsAccountID,
                        principalTable: "Accounts",
                        principalColumn: "AccountID");
                    table.ForeignKey(
                        name: "FK_Reports_Logss_LogsID",
                        column: x => x.LogsID,
                        principalTable: "Logss",
                        principalColumn: "LogsID",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_Reports_Transactions_TransactionsTransactionalID",
                        column: x => x.TransactionsTransactionalID,
                        principalTable: "Transactions",
                        principalColumn: "TransactionalID");
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_CustomerCustumerID",
                table: "Accounts",
                column: "CustomerCustumerID");

            migrationBuilder.CreateIndex(
                name: "IX_Logss_TransactionsTransactionalID",
                table: "Logss",
                column: "TransactionsTransactionalID");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_AccountsAccountID",
                table: "Reports",
                column: "AccountsAccountID");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_LogsID",
                table: "Reports",
                column: "LogsID");

            migrationBuilder.CreateIndex(
                name: "IX_Reports_TransactionsTransactionalID",
                table: "Reports",
                column: "TransactionsTransactionalID");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_CustomerID",
                table: "Transactions",
                column: "CustomerID");

            migrationBuilder.CreateIndex(
                name: "IX_Transactions_EmployeesEmployeeID",
                table: "Transactions",
                column: "EmployeesEmployeeID");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Reports");

            migrationBuilder.DropTable(
                name: "Accounts");

            migrationBuilder.DropTable(
                name: "Logss");

            migrationBuilder.DropTable(
                name: "Transactions");

            migrationBuilder.DropTable(
                name: "Customers");

            migrationBuilder.DropTable(
                name: "Employeess");
        }
    }
}
